package ru.kastricyn.santa.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.kastricyn.generated.controller.GroupApi;
import ru.kastricyn.generated.dto.GroupDto;
import ru.kastricyn.generated.dto.GroupShortDto;
import ru.kastricyn.generated.dto.ParticipantDto;
import ru.kastricyn.generated.dto.ParticipantShortDto;
import ru.kastricyn.santa.model.Participant;
import ru.kastricyn.santa.service.GroupService;
import ru.kastricyn.santa.service.ParticipantService;

import java.util.List;

import static ru.kastricyn.santa.controllers.ResponseEntityAdapter.re201;
import static ru.kastricyn.santa.controllers.ResponseEntityAdapter.reok;

@RestController
@AllArgsConstructor
public class GroupController implements GroupApi {
    private GroupService groupService;
    private ParticipantService participantService;

    @Override
    public ResponseEntity<Void> deleteGroup(Long groupId) {
        groupService.deleteGroup(groupId);
        return reok();
    }

    @Override
    public ResponseEntity<Void> deleteParticipant(Long groupId, Long participantId) {
        groupService.deleteParticipantFromGroup(participantId, groupId);
        return reok();
    }

    @Override
    public ResponseEntity<GroupDto> getGroup(Long groupId) {
        return reok(groupService.getGroupById(groupId).dto());
    }

    @Override
    public ResponseEntity<ParticipantShortDto> getRecipient(Long groupId, Long participantId) {
        return reok(participantService.getRecipient(groupId, participantId).shortDto());
    }

    @Override
    public ResponseEntity<Long> postGroup(GroupShortDto groupForCreate) {
        return re201(groupService.createGroup(groupForCreate).getId());
    }

    @Override
    public ResponseEntity<List<ParticipantDto>> postGroupToss(Long groupId) {
        groupService.toss(groupId);
        return reok(
                groupService.getGroupById(groupId).getParticipants().stream().map(Participant::dto).toList()
        );
    }

    @Override
    public ResponseEntity<Long> postParticipant(Long groupId, ParticipantShortDto participantForCreate) {
        return reok(groupService.addParticipant(groupId, participantForCreate));
    }

    @Override
    public ResponseEntity<GroupDto> putGroup(Long groupId, GroupShortDto groupForCreate) {
        groupService.updateGroup(groupId, groupForCreate);
        return reok(groupService.getGroupById(groupId).dto());
    }
}
