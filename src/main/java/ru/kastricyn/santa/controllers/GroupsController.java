package ru.kastricyn.santa.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.kastricyn.generated.controller.GroupsApi;
import ru.kastricyn.generated.dto.GroupShortDto;
import ru.kastricyn.santa.model.Group;
import ru.kastricyn.santa.service.GroupService;

import java.util.List;

import static ru.kastricyn.santa.controllers.ResponseEntityAdapter.reok;

@RestController
@AllArgsConstructor
public class GroupsController implements GroupsApi {
    private GroupService groupService;

    @Override
    public ResponseEntity<List<GroupShortDto>> getGroupList() {
        return reok(groupService.getAllGroups().stream().map(Group::shortDro).toList());
    }
}
