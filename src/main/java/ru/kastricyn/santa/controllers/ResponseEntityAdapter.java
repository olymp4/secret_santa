package ru.kastricyn.santa.controllers;

import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

public class ResponseEntityAdapter {
    public static <T> ResponseEntity<T> reok(T value) {
        return new ResponseEntity<>(value, HttpStatusCode.valueOf(200));
    }
    public static  ResponseEntity<Void> reok() {
        return new ResponseEntity<>(HttpStatusCode.valueOf(200));
    }

    public static <T> ResponseEntity<T> re201(T value) {
        return new ResponseEntity<>(value, HttpStatusCode.valueOf(201));
    }
    public static <T> ResponseEntity<T> re400(T value) {
        return new ResponseEntity<>(value, HttpStatusCode.valueOf(400));
    }

    public static <T> ResponseEntity<T> re403(T value) {
        return new ResponseEntity<>(value, HttpStatusCode.valueOf(403));
    }

    public static <T> ResponseEntity<T> re404(T value) {
        return new ResponseEntity<>(value, HttpStatusCode.valueOf(404));
    }
    public static <T> ResponseEntity<T> re409(T value) {
        return new ResponseEntity<>(value, HttpStatusCode.valueOf(409));
    }

    public static <T> ResponseEntity<T> re(T value, int code) {
        return new ResponseEntity<>(value, HttpStatusCode.valueOf(code));
    }
}
