package ru.kastricyn.santa.exceptions;

public class ConflictException extends RuntimeException{
    public ConflictException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "ConflictException{" + getMessage() + "}";
    }

}
