package ru.kastricyn.santa.exceptions;

public class InvalidParametersException extends IllegalArgumentException {
    public InvalidParametersException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "InvalidParametersException{" + getMessage() + "}";
    }
}
