package ru.kastricyn.santa.exceptions;

import lombok.AllArgsConstructor;


public class ObjectNotExistException extends RuntimeException {
    public ObjectNotExistException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "ObjectNotExistException{" + getMessage() + "}";
    }
}
