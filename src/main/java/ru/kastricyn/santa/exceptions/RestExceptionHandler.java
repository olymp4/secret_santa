package ru.kastricyn.santa.exceptions;

import com.fasterxml.jackson.databind.JsonMappingException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import ru.kastricyn.generated.dto.Response400Dto;
import ru.kastricyn.generated.dto.Response404Dto;
import ru.kastricyn.generated.dto.Response409Dto;

import static ru.kastricyn.santa.controllers.ResponseEntityAdapter.re400;
import static ru.kastricyn.santa.controllers.ResponseEntityAdapter.re404;
import static ru.kastricyn.santa.controllers.ResponseEntityAdapter.re409;

@ControllerAdvice(annotations = RestController.class)
public class RestExceptionHandler {
    @ExceptionHandler(ObjectNotExistException.class)
    public ResponseEntity<Response404Dto> handleObjectNotExits(ObjectNotExistException e) {
        return re404(new Response404Dto()
                .code(404)
                .description("Not found")
                .reason(e.toString())
        );
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Response400Dto> handleInvalidParameters(IllegalArgumentException e) {
        return re400(new Response400Dto()
                .code(400)
                .description("Bad request")
                .reason(e.toString())
        );
    }

    @ExceptionHandler({ConflictException.class})
    public ResponseEntity<Response409Dto> handleInvalidParameters(ConflictException e) {
        return re409(new Response409Dto()
                .code(409)
                .description("Conflict")
                .reason(e.toString())
        );
    }

    @ExceptionHandler({JsonMappingException.class})
    public ResponseEntity<Response400Dto> handleUnknownProperties(JsonMappingException e) {
        return re400(new Response400Dto()
                .code(400)
                .description("Bad request")
                .reason("Какой-то неизвестный объект, проверьте, что все поля, как в спецификации.")
        );
    }

}
