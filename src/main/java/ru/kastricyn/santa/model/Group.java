package ru.kastricyn.santa.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;
import ru.kastricyn.generated.dto.GroupDto;
import ru.kastricyn.generated.dto.GroupShortDto;
import ru.kastricyn.santa.exceptions.InvalidParametersException;
import ru.kastricyn.santa.exceptions.ObjectNotExistException;

import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description = null;
    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Participant> participants = new ArrayList<>();

    public void removeParticipantById(long participantId) {
        boolean flag = true;
        var it = participants.iterator();
        while (it.hasNext()) {
            var cur = it.next();
            if (cur.getId() == participantId) {
                cur.setGroup(null);
                it.remove();
                flag = false;
            }
        }
        if (flag)
            throw new ObjectNotExistException("участник с id %d не найден в группе с id %d".formatted(participantId,
                    id));
    }

    public void clear() {
        participants.forEach(it -> it.setGroup(null));
        participants.clear();
    }

    public GroupDto dto() {
        return new GroupDto()
                .id(id)
                .name(name)
                .description(description)
                .participants(
                        participants.stream().map(Participant::shortDto).toList()
                );
    }

    public GroupShortDto shortDro() {
        return new GroupShortDto()
                .id(id)
                .name(name)
                .description(description);
    }

    public void setName(String name) {
        if (name == null || name.isEmpty())
            throw new InvalidParametersException("поле name не может быть null или пустым");
        this.name = name;
    }
}