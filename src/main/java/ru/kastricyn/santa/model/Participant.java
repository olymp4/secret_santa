package ru.kastricyn.santa.model;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.Getter;
import lombok.Setter;
import ru.kastricyn.generated.dto.ParticipantDto;
import ru.kastricyn.generated.dto.ParticipantShortDto;
import ru.kastricyn.santa.exceptions.InvalidParametersException;

@Entity
@Getter
@Setter
public class Participant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String wish;
    @ManyToOne(fetch = FetchType.LAZY)
    private Group group;
    @OneToOne
    private Participant recipient;

    public ParticipantDto dto() {
        return new ParticipantDto()
                .id(id)
                .name(name)
                .wish(wish)
                .recipient(recipient.shortDto());
    }

    public ParticipantShortDto shortDto() {
        return new ParticipantShortDto()
                .id(id)
                .name(name)
                .wish(wish);
    }

    public void setName(String name){
        if (name == null || name.isEmpty())
            throw new InvalidParametersException("поле name не может быть null или пустым");
        this.name = name;
    }
}
