package ru.kastricyn.santa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kastricyn.santa.model.Group;

public interface GroupRepository extends JpaRepository<Group, Long> {
}
