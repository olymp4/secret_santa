package ru.kastricyn.santa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kastricyn.santa.model.Participant;

import java.util.Optional;

public interface ParticipantRepository extends JpaRepository<Participant, Long> {
    Optional<Participant> findByIdAndGroupId(Long participantId, Long groupId);
}