package ru.kastricyn.santa.service;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.kastricyn.generated.dto.GroupShortDto;
import ru.kastricyn.generated.dto.ParticipantShortDto;
import ru.kastricyn.santa.exceptions.ConflictException;
import ru.kastricyn.santa.exceptions.ObjectNotExistException;
import ru.kastricyn.santa.model.Group;
import ru.kastricyn.santa.repository.GroupRepository;

import java.util.List;

@AllArgsConstructor
@Service
public class GroupService {
    private GroupRepository groupRepository;
    private ParticipantService participantService;

    @Transactional
    public void deleteGroup(Long groupId) {
        getGroupById(groupId);
        groupRepository.deleteById(groupId);
    }

    @Transactional
    public void deleteParticipantFromGroup(Long participantId, Long groupId) {
        var group = getGroupById(groupId);
        group.removeParticipantById(participantId);
    }

    public @NotNull Group getGroupById(Long groupId) {
        var group = groupRepository.findById(groupId);
        if (group.isEmpty())
            throw new ObjectNotExistException("не нашлось группы с id = " + groupId);
        return group.get();
    }

    @Transactional
    public Group createGroup(GroupShortDto groupForCreate) {
        var group = new Group();
        group.setName(groupForCreate.getName());
        group.setDescription(groupForCreate.getDescription());
        return groupRepository.save(group);
    }

    @Transactional
    public void toss(Long groupId) {
        var participants = getGroupById(groupId).getParticipants();
        if (participants.size() < 3)
            throw new ConflictException("Проведение жеребьевки возможно только в том случае, когда количество " +
                    "участников группы >= 3");
        for (var i = 1; i < participants.size(); i++)
            participants.get(i - 1).setRecipient(participants.get(i));
        participants.get(participants.size() - 1).setRecipient(participants.get(0));
    }

    @Transactional
    public Long addParticipant(Long groupId, ParticipantShortDto participantForCreate) {
        var group = getGroupById(groupId);
        var participant = participantService.createParticipant(participantForCreate);
        participant.setGroup(group);
        group.getParticipants().add(participant);
        return participant.getId();
    }

    @Transactional
    public void updateGroup(Long groupId, GroupShortDto groupForCreate) {
        var group = getGroupById(groupId);
        group.setName(groupForCreate.getName());
        group.setDescription(groupForCreate.getDescription());
    }

    public List<Group> getAllGroups() {
        return groupRepository.findAll();
    }
}
