package ru.kastricyn.santa.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kastricyn.generated.dto.ParticipantShortDto;
import ru.kastricyn.santa.exceptions.ObjectNotExistException;
import ru.kastricyn.santa.model.Participant;
import ru.kastricyn.santa.repository.ParticipantRepository;

@AllArgsConstructor
@Service
public class ParticipantService {
    private ParticipantRepository participantRepository;

    public Participant getRecipient(Long groupId, Long participantId) {
        var participant = participantRepository.findByIdAndGroupId(participantId, groupId);
        if (participant.isEmpty())
            throw new ObjectNotExistException("участник с id %d не найден в группе с id %d".formatted(participantId,
                    groupId));
        return participant.get();
    }

    public Participant createParticipant(ParticipantShortDto participantForCreate) {
        var participant = new Participant();
        participant.setName(participantForCreate.getName());
        participant.setWish(participantForCreate.getWish());
        return participantRepository.save(participant);
    }

}